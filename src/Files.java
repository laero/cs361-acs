
public class Files {
	public String name;
	public Users owner;
	public String mode;

	public Files(String n, Users o, String m)
	{
		name = n;
		owner = o;
		mode = "";
		char temp[] = m.toCharArray();
		for (int i = 0; i < 4; i++)
		{
			if (temp[i] == '0')
				mode += "000";
			else if (temp[i] == '1')
				mode += "001";
			else if (temp[i] == '2')
				mode += "010";
			else if (temp[i] == '3')
				mode += "011";
			else
				mode += Integer.toBinaryString(Integer.parseInt(temp[i] + ""));
		}
	}

	public void print()
	{
		System.out.println(name + " " + owner.name + " " + mode);
	}

	public void chmod(String m)
	{
		mode = "";
		char temp[] = m.toCharArray();
		for (int i = 0; i < 4; i++)
		{
			if (temp[i] == '0')
				mode += "000";
			else if (temp[i] == '1')
				mode += "001";
			else if (temp[i] == '2')
				mode += "010";
			else if (temp[i] == '3')
				mode += "011";
			else
				mode += Integer.toBinaryString(Integer.parseInt(temp[i] + ""));
		}
	}

	public String exitState()
	{
		char state[] = new char[9];
		for (int i = 3; i < 12; i++)
		{
			if (i % 3 == 0)
			{
				if (mode.charAt(i) == '1')
					state[i-3] = 'r';
				else
					state[i-3] = '-';
			}
			if (i % 3 == 1)
			{
				if (mode.charAt(i) == '1')
					state[i-3] = 'w';
				else
					state[i-3] = '-';
			}
			if (i % 3 == 2)
			{
				if (mode.charAt(i) == '1')
					state[i-3] = 'x';
				else
					state[i-3] = '-';
			}
		}
		
		if (mode.charAt(0) == '1')
		{
			if (mode.charAt(5) == '1')
				state[2] = 's';
			else
				state[2] = 'S';
		}
		if (mode.charAt(1) == '1')
		{
			if (mode.charAt(8) == '1')
				state[5] = 's';
			else
				state[5] = 'S';
		}
		if (mode.charAt(2) == '1')
		{
			if (mode.charAt(11) == '1')
				state[8] = 't';
			else
				state[8] = 'T';
		}
		
		String ret = new String(state);
		ret += " " + owner.name + " " + owner.group + " " + name;
		System.out.println(ret);
		return ret;
	}

}
