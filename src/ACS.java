import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

public class ACS {
	
	static boolean rootAccess;
	static File fileUser;
	static File fileList;
	static File fileState;
	static ArrayList<Users> listUsers;
	static ArrayList<Files> listFiles;
	static BufferedWriter statebw;
	static FileWriter statefw;
	
//	java ACS option userList fileList
	public static void main(String args[]) throws IOException
	{
		createFile("state.log");
		readArgs(args);
		readFiles();
		
		if(rootAccess)
		{
			Users root = new Users("root", "root");
			listUsers.add(root);
		}
		
		// execute actions
		printData();
		waitForInput();
		exitAction();
		statebw.close();
		statefw.close();
	}
	
	public static void waitForInput()
	{
		String line;
		Scanner scanner = new Scanner (System.in);
		while (!(line = scanner.nextLine().toLowerCase()).equals("exit"))
		{
//			System.out.println(line);
			checkAction(line);
		}
		scanner.close();
	}
	
	public static void exitAction()
	{
		for (int i = 0; i < listFiles.size(); i++)
		{
			try {
				statebw.write(listFiles.get(i).exitState() + '\n');
			} catch (IOException e) {
			}
		}
	}
	
//	action user file
	public static void checkAction(String l)
	{	
		int level = 0;
		char bit = '0';
		String actions[] = l.split(" ");
		Users u = findUser(actions[1]);
		Files f = findFile(actions[2]);
		
		String user = u.name;
		String group = u.group;
		
		if (u == null || f == null)
		{
			System.out.println(l + " 0");
			return;
		}
		if (u.name.equals(f.owner.name))
			level = 1;	//user found
		else if (u.group.equals(f.owner.group))
			level = 2;	//group found
		else			
			level = 3;	//other found
		
		if (actions[0].equals("read"))
		{
			bit = f.mode.charAt(level * 3);
		}
		else if (actions[0].equals("write"))
		{
			bit = f.mode.charAt(level * 3 + 1);
		}
		else if (actions[0].equals("execute"))
		{
			bit = f.mode.charAt(level * 3 + 2);
			if(level != 1)
			{
				if (f.mode.charAt(0) == '1')
				{
					user = f.owner.name;
				}
				if (f.mode.charAt(1) == '1')
				{
					group = f.owner.group;
				}
				
			}
		}
		else if (actions[0].equals("chmod"))
		{
			if (actions[3].length() != 4)
			{
				System.out.println(l + " 0");
				System.out.println("WRN");
				return;
			}
			if(level == 1 || u.name.equals("root"))
			{
				bit = '1';
				f.chmod(actions[3]);
			}
		}
		else
		{
		}
		
		if(u.name.equals("root"))
			bit = '1';
		
		System.out.println(actions[0] + " " + user + " " + group + " " + bit);
	}
	
	public static Files findFile(String f)
	{
		for (int i = 0; i < listFiles.size(); i++)
			if (listFiles.get(i).name.equals(f))
				return listFiles.get(i);
		return null;
	}
	
	public static Users findUser(String u)
	{
		for (int i = 0; i < listUsers.size(); i++)
			if (listUsers.get(i).name.equals(u))
				return listUsers.get(i);
		return null;
	}
	
	public static void printData()
	{
		for (int i = 0; i < listUsers.size(); i++)
		{
			listUsers.get(i).print();
		}
		
		for (int i = 0; i < listFiles.size(); i++)
		{
			listFiles.get(i).print();
		}
	}
	
	public static void readArgs(String args[])
	{
		if(args.length == 2)
		{
			rootAccess = true;
			fileUser = new File(args[0]);
			fileList = new File(args[1]);
		}
		else if(args.length == 3)
		{
			rootAccess = false;
			fileUser = new File(args[1]);
			fileList = new File(args[2]);
		}
		else
		{
			System.out.println("Bad arguments");
		}
	}
	
	public static void readFiles() {
		listUsers = new ArrayList<Users>();
		listFiles = new ArrayList<Files>();
		BufferedReader brUsers = null;
		BufferedReader brFiles = null;
		try {
			brUsers = new BufferedReader(new FileReader(fileUser));
			brFiles = new BufferedReader(new FileReader(fileList));
		} catch (FileNotFoundException e) {
			System.out.println("Error: File(s) not found.");
			System.exit(0);
		}
		String line;
		String temp[];
		try {
			while ((line = brUsers.readLine()) != null)
			{
				temp = line.split(" ");
				Users u = new Users(temp[0], temp[1]);
				listUsers.add(u);
			}
			while ((line = brFiles.readLine()) != null)
			{
				temp = line.split(" ");
				Files f = new Files(temp[0], findUser(temp[1]), temp[2]);
				listFiles.add(f);
			}
		} catch (IOException e) {
		}
	}

	public static void createFile(String s) {
		fileState = new File(s);
		try {
			if (!fileState.exists()) 
				fileState.createNewFile();
			statefw = new FileWriter(fileState);
			statebw = new BufferedWriter(statefw);
		}
		catch (IOException e) {}
	}
	
	
}
